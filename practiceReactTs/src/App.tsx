
import './App.css'
import TestProp from './testProp'
import HeadingProps from './headingProps'
import Button from './Button'
import Input from './Input'
import Container from './Container'
import Counter from './state/Counter'
import Box from './Context/Box'
import ThemeContextProvider from './Context/ThemeContext'
function App() {


  return (
    <>
    <h1>hello</h1>
    < TestProp name={{first: 'brijesh', last: 'kumar'}}/>
    {/* //advance Props */}
    <HeadingProps>hello</HeadingProps>
    <Button handleClick={() => console.log('buton clicked')} />
    <Input value='' handleChange={(event) => console.log(event.target.value)}/>
    <Container styles={{border:'1px solid black', padding:'1rem', display:0}}/>
    <Counter/>
    <ThemeContextProvider>
      <Box/>
    </ThemeContextProvider>
    
    </>
  )
}

export default App
