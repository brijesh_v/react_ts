import { useState } from "react"
type AuthUser = {
    name: string
    email: string
}
export default function User() {
    const [user, setUser] = useState<AuthUser | null>(null)
    const handleLogin = ()=>{
        setUser({
            name:'Brijesh',
            email:'brijeshvishwakarma@nimapinfotech.com'
        })
    }
    const handleLogout = ()=>{
        setUser(null)
    }

  return (
    <div>

    <button onClick={handleLogin}>Login </button>
    <button onClick={handleLogout}>Logout </button>
    <div>User is name is {user?.name} </div>
    <div>User is name is {user?.email} </div>



    </div>
  )
}
