import { createContext } from "react";
import { Theme } from "./Theme";

type ThemecontextProps = {
   children:React.ReactNode
}

export const ThemeContext = createContext(Theme)


export default function ThemeContextProvider({children}:ThemecontextProps) {
  return <ThemeContext.Provider value={Theme}>{children}</ThemeContext.Provider>
}
