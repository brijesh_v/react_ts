import React from "react"

type CotainerProps ={
    styles:React.CSSProperties
}
function Container(props:CotainerProps) {
  return (
    <div style={props.styles}>Text content</div>
  )
}

export default Container