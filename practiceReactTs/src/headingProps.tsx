type HeadingProps = 
{
 children: string   
}
export default function HeadingProps(props: HeadingProps) {
  return (
    <div>{props.children}</div>
  )
}
