import './App.css'
import Header from './Components/Header'
import TodoItem from './Components/TodoItem'
import Button from './Components/Button'
import './style.css'
function App() {

  return (
    <div className='todo-container'>
    <Header/>
    <TodoItem text='Eat' completed={false}/>
    <TodoItem text='Code'completed={false} />
    <TodoItem text='Play'completed={false} />
    <TodoItem text='Study' completed={false} />
    <TodoItem text='Repeat'completed={false} />

    <Button/>
     </div>
  )
}

export default App
