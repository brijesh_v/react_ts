type TodoItemProps = {
    text:string
    completed:boolean
}
export default function TodoItem(props:TodoItemProps){
    return (
        <li className="todo-item">
        <span>       
        {props.completed ? <></> : <input type="checkbox" />}
        <span className="todo-item-text">{props.text}</span>
        </span>
        <p>...</p>
        </li>
    )
}