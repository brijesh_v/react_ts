import React, { createContext, useState } from 'react'
// import { CounterStateContext } from './CounterState'
type CounterContextType = {
    count: number;
    setCount: React.Dispatch<React.SetStateAction<{ count: number }>>;
};
export const CounterStateContext = createContext<CounterContextType | null>(null);
type CounterProps = {
    children:React.ReactNode
}
const CounterIntialState = {count:0}  
export default function CountersContext(Props:CounterProps) {
    const [Statevalue, setValue] = useState(CounterIntialState)
    return (
        <CounterStateContext.Provider value={{count:Statevalue.count, setCount:setValue}}>
            {Props.children}
        </CounterStateContext.Provider>
    );
}

