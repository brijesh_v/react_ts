import React, { createContext, useState } from 'react'

export const CounterStateContext = createContext<number | null>(null);
