import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import CountersContext from './Context/Counter.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <CountersContext>
    <App />
    </CountersContext>

  </React.StrictMode>,
)