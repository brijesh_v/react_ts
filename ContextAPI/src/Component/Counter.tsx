import  { useContext } from 'react'
import { CounterStateContext } from '../Context/Counter'

export default function Counter() {
  
   const counterContext = useContext(CounterStateContext);

  if (!counterContext) {
      return <div>Loading...</div>; // Handle null case
  }
  return (
    
    <div>
 <button onClick={() => counterContext.setCount({ count: counterContext.count + 1 })}>
                Increment
            </button>        
            <button onClick={() => counterContext.setCount({ count: counterContext.count - 1 })}>
                Decrement
            </button>

    </div>
  )
}
