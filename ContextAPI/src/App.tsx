
import { useContext } from 'react'
import './App.css'
import Counter from './Component/Counter'
import { CounterStateContext } from './Context/Counter'
function App() {
const counterState = useContext(CounterStateContext)
console.log('counterState', counterState)
  return (
    <>
    <div className='App'></div>
    {/* <h1>Count is {counterState.count}</h1> */}
    <h1>Count is {counterState?.count}</h1>
    <Counter/>
    <Counter/>
    <Counter/>
    <Counter/>

         </>
  )
}

export default App
